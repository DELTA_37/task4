#include <sys/socket.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>

#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xlib.h>

#define PORT (8080)
#define CLNUM (100)
#define BUFSIZE (16)
#define WAITVAL (10000)

using namespace std;

vector<string> d;

void Do(const char* buf);

void Do(char* buf)
{
	/*
	for (int i = 0; i < BUFSIZE; i++)
	{
		if (buf[i] == 'a')
		{
			buf[i] = '\0';
		}
	}
	*/
	printf("%s", buf);
	/*
	if (buf[strlen(buf) - 1] == '\n')
	{
		buf[strlen(buf) - 1] = '\0';
	}
	d.push_back(buf);
	*/
}

int main(void)
{
	// work with X11
	Display* disp = XOpenDisplay(NULL);

	int screen = XDefaultScreen(disp);

	Window root = XRootWindow(disp, screen);

	unsigned long black = XBlackPixel(disp, screen);
	unsigned long white = XWhitePixel(disp, screen);

	Window wind = XCreateSimpleWindow
		(
			disp, root,
			0, 0, 500, 500, 0,
			black, white
		);

	XMapWindow(disp, wind);
	XFlush(disp);
	// end
	
	// work with sockets
	int sock;
	int client[CLNUM];
	int num = 0;
	
	sockaddr_in addr;

	bzero(&addr, sizeof(addr));
	addr.sin_port = htons(PORT);
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;

	sock = socket(AF_INET, SOCK_STREAM, 0);

	assert(sock > 0);

	bind(sock, (sockaddr*)&addr, sizeof(addr));

	listen(sock, CLNUM);

	fd_set set;
	FD_ZERO(&set);
	FD_SET(sock, &set);

	timeval t_param;
	t_param.tv_sec = 0;
	t_param.tv_usec = 0;
	timeval t_param3;
	t_param.tv_sec = 3;
	t_param.tv_usec = 0;
	// end
	
	// GC init
	XGCValues value;	
	unsigned long valuemask;
	GC gc;

	value.foreground = 0;
	value.line_width = 3;
	value.fill_style = FillSolid;
	value.line_style = LineSolid;

	valuemask = GCLineStyle | GCLineWidth | GCFillStyle | GCForeground;

	gc = XCreateGC(disp, wind, valuemask, &value);
	// end
	
	// text GC init
	
	XGCValues tvalue;	
	unsigned long tvaluemask;
	GC tgc;

	tvalue.foreground = 0;
	tvalue.font = XLoadFont(disp, "rk24");
	tvalue.line_width = 14;

	tvaluemask = GCFont | GCLineWidth | GCForeground;

	tgc = XCreateGC(disp, wind, tvaluemask, &tvalue);
	// end


	while(1)
	{
		// event from sockets
		fd_set setr = set;
		fd_set sete = set;
		
		if (select(FD_SETSIZE, &setr, NULL, &sete, &t_param) > 0)
		{
			if (FD_ISSET(sock, &setr))
			{
				int nsock = accept(sock, NULL, NULL);
				if (num == CLNUM - 1)
				{
					printf("I have killed this client\n");
				}
				else
				{
					client[num] = nsock;
					FD_SET(nsock, &set);
					num++;
				}
			}

			for (int i = 0; i < num; i++)
			{
				if (FD_ISSET(client[i], &setr)  && !FD_ISSET(client[i],&sete))
				{
					printf("Reading: %d\n", client[i]);

					char buf[BUFSIZE];
					bzero(buf, BUFSIZE);

					int r = 0;

					int t = 0;

					while (r < BUFSIZE)
					{
						fd_set dopset,dopsete;

						FD_ZERO(&dopset);
						FD_SET(client[i], &dopset);
						FD_ZERO(&dopsete);
						FD_SET(client[i], &dopsete);

						int rtn=select(FD_SETSIZE, &dopset, NULL, &dopsete, &t_param3);
						int rtn2=FD_ISSET(client[i],&dopset);
						int rtn3=FD_ISSET(client[i],&dopsete);
						/*
                                                if(rtn<=0||(!rtn2)||rtn3)
                                                {
                                                 printf("ERRRRRRRRRRRRRRRRR\n");
                                                }
						printf("rtn=%d rtn2=%d rtn3=%d\n",rtn,rtn2,rtn3);
						*/
						if (FD_ISSET(client[i], &dopsete))
						{
							printf("Connection failed with: %d\n", client[i]);
							break;
						}
						
						r += recv(client[i], buf + r, BUFSIZE, 0);

						t++;
						if (t > WAITVAL)
						{
							printf("Time is lost: %d\n", client[i]);
							break;
						}
					}
					printf("%s\n", buf);
					sleep(1);
					Do(buf);
				}

				if (FD_ISSET(client[i], &sete))
				{
					printf("Closing: %d\n", client[i]);
					close(client[i]);
					FD_CLR(client[i], &set);
					for (int j = i; j < num; j++)
					{
						client[j] = client[j + 1];
					}
					client[num - 1] = 0;
					num--;
				}
			}
		}
		// end

		// events from X11
		XEvent event;

		XCheckMaskEvent(disp, ExposureMask, &event);

		switch(event.type)
		{
			case Expose:
			{
				/*
				 * Что уже нарисовано
				 */
				
				XClearWindow(disp, wind);

				for (int i = 0; i < d.size(); i++)
				{
					if ((d[i].size() > 4)&&(d[i].substr(0, 4) == "line"))
					{
						istringstream cur(d[i].substr(5));
						int x1, x2, y1, y2;
						cur >> x1 >> y1 >> x2 >> y2;
						XDrawLine(disp, wind, gc, x1, y1, x2, y2);
						XFlush(disp);
					}
				}

				
				XFlush(disp);
				break;
			}
		}

		// end
	}

	close(sock);
	return 0;
}
