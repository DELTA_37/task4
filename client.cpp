#include <sys/socket.h>
#include <iostream>
#include <string>
#include <vector>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>

#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xlib.h>

#define PORT (8080)
#define BUFSIZE (16)
#define SERVER_ADDR "127.0.0.1"

int main(void)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);

	sockaddr_in addr;

	bzero(&addr, sizeof(addr));
	addr.sin_port = htons(PORT);
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(SERVER_ADDR);

	sock = socket(AF_INET, SOCK_STREAM, 0);

	assert(sock > 0);

	if (connect(sock, (sockaddr*)&addr, sizeof(addr)) == -1)
	{
		printf("[error]");
	}
	
	char buf[BUFSIZE];

	bzero(buf, BUFSIZE);
	strcpy(buf, "line 0 0 100 154");
	
	write(sock, buf, BUFSIZE);

	close(sock);
	return 0;
}
