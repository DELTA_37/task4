all: server client

server: server.cpp
	g++ server.cpp -lX11 -g -o server
client: client.cpp
	g++ client.cpp -lX11 -g -o client

clean:
	rm *.o
	rm server 
	rm client
